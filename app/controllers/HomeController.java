package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import models.Repository;
import play.cache.Cached;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

	
	private static final List<String> styles = new ArrayList<>();
	static {
		styles.add("success");
		styles.add("warning");
		styles.add("danger");
		styles.add("info");
		styles.add("primary");
	}
	
	@Cached(key="index")
    public Result index() {
    	try {
			List<Map<String, Object>> repos = helpers.GitHub.getRepositories();
			
			
			return ok(views.html.index.render( repos ));
			
		} catch (UnirestException e) {
			e.printStackTrace();
			return internalServerError("Ooops, something went wrong!");
		}
    }

    public Result viewRepository(String repo) {
    	
    	
    	try {
			Repository repoOb = helpers.GitHub.getRepository(repo);
			if( repoOb.getId() == null )
				return notFound("Oops, we couldn't find this repository");
			
			//Can also use the url provided in the previous response
			Map<String, Integer> langs = helpers.GitHub.getRepoLanguages(repo);
			
			Long totalLines = 0l;
			Map<String, String> langColourMap = new HashMap<>();
			int counter = 0;
			for(String key: langs.keySet()) {
				try {
					totalLines += langs.get(key).longValue();
					langColourMap.put(key, styles.get( counter++ ) );
				} catch(NumberFormatException e) {
					
				}
			}
			return ok( views.html.repo.render(repoOb, langs, langColourMap, totalLines) );
		} catch (UnirestException e) {
			e.printStackTrace();
			return internalServerError("Ooops, something went wrong!");
		}
    	
    }

}
