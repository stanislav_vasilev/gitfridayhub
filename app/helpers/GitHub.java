package helpers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import models.Repository;

public class GitHub {

	private static final String apiPrefix = "https://api.github.com";
	private static final String username = "nasa";
	
	private static final String reposListUrl = apiPrefix+"/users/{username}/repos";
	private static final String repoUrl = apiPrefix+"/repos/{username}/{repo}";
	private static final String repoLanguagesUrl = apiPrefix+"/repos/{username}/{repo}/languages";
	
	static {
		Unirest.setObjectMapper(new ObjectMapper() {
			private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
			
			public <T> T readValue(String value, Class<T> valueType) {
				try {
					return jacksonObjectMapper.readValue(value, valueType);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			
			public String writeValue(Object value) {
				try {
					return jacksonObjectMapper.writeValueAsString(value);
				} catch (JsonProcessingException e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	public static List<Map<String, Object>> getRepositories() throws UnirestException {
		HttpResponse<List> reposResponse = Unirest.get( reposListUrl )
							    			.routeParam("username", username)
							    			.asObject(List.class)
							    			;
		List<Map<String, Object>> repos = reposResponse.getBody();
		return repos;
	}
	
	public static Map<String, Integer> getRepoLanguages(String repo) throws UnirestException {
		HttpResponse<Map> languagesResponse = Unirest.get( repoLanguagesUrl )
														    			.routeParam("username", username)
														    			.routeParam("repo", repo)
														    			.asObject(Map.class)
														    			;
		Map<String, Integer> langs = languagesResponse.getBody();
		return langs;
	}


	public static Repository getRepository(String repo) throws UnirestException {
		HttpResponse<Repository> reposResponse = Unirest.get( repoUrl )
										    			.routeParam("username", username)
										    			.routeParam("repo", repo)
										    			.asObject(Repository.class)
										    			;
		Repository repoOb = reposResponse.getBody();
		return repoOb;
	}
	
}
