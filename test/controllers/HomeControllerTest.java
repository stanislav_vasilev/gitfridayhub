package controllers;

import org.junit.Test;

import com.mashape.unirest.http.exceptions.UnirestException;

import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Http.Status;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.GET;
import static play.test.Helpers.route;

import java.util.List;
import java.util.Map;

public class HomeControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void testIndex() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/");

        Result result = route(app, request);
        assertEquals(OK, result.status());
    }
    
    @Test
    public void testRepoPage() {
    	Http.RequestBuilder request = new Http.RequestBuilder()
    			.method(GET)
    			.uri("/repository/digital-strategy");
    	
    	Result result = route(app, request);
    	assertEquals(OK, result.status());
    	
    	request = new Http.RequestBuilder()
    			.method(GET)
    			.uri("/repository/digital-strategy-bad");
    	
    	result = route(app, request);
    	assertEquals(Status.NOT_FOUND, result.status());
    }
    
    
    
}
