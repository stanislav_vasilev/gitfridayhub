package controllers;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.mashape.unirest.http.exceptions.UnirestException;

import models.Repository;

public class HelperTest {

	@Test
    public void testGetRepositories() {
    	try {
			List<Map<String, Object>> repos = helpers.GitHub.getRepositories();
			assertEquals(true, repos instanceof List);
			assertEquals(false, repos.isEmpty());
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Test
	public void testGetRepo() {
		try {
			Repository repoOb = helpers.GitHub.getRepository("GTM_DesignSim");
			assertEquals(true, repoOb.getId() != null && repoOb.getId() > 0);
			assertEquals(true, repoOb instanceof Repository);
			
			
			
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetRepoLanguages() {
		try {
			Map<String, Integer> langs = helpers.GitHub.getRepoLanguages("GTM_DesignSim");
			assertEquals(true, langs.keySet().size() > 0);
			assertEquals(true, langs.containsKey("Matlab"));
			assertEquals(true, langs.get("Matlab") > 0);
			
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
