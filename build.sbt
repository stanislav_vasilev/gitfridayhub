name := """WeAreFridayTest"""
organization := "net.iamstan"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, SbtWeb)

JsEngineKeys.engineType := JsEngineKeys.EngineType.Node

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.webjars" % "webjars-play_2.11" % "2.5.0-4",
  "org.webjars.npm" % "jquery" % "3.1.1",
  "org.webjars" % "bootstrap" % "3.3.6",
  "com.mashape.unirest" % "unirest-java" % "1.4.9"
)

libraryDependencies += filters


EclipseKeys.preTasks := Seq(compile in Compile)
EclipseKeys.projectFlavor := EclipseProjectFlavor.Java           // Java project. Don't expect Scala IDE
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(EclipseCreateSrc.ManagedClasses, EclipseCreateSrc.ManagedResources)  // Use .class files instead of generated .scala files for views and routes 


includeFilter in (Assets, LessKeys.less) := "*.less"
LessKeys.compress := true
excludeFilter in (Assets, LessKeys.less) := "_*.less"




